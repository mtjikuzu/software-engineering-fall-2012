package edu.luc.rrs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Purchase {

	Operations transaction = new Operations();
	private int input;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	private boolean flag = false;
	Application app;
	public void purchasingInterface()
	{

		System.out.println("*********************************************************************");
		System.out.println("*Welcome to Devastating Deliverables Store Purchasing Interface******");
		System.out.println("Please enter the products you would like to buy in the prompts boxes*");
		System.out.println("*********************************************************************");
		
			try{
				do{						
					System.out.println("Enter product id: ");
					input = Integer.parseInt(br.readLine());
					Product product =  transaction.loadProduct(input, flag);				
					if(product == null)
					{
						//
						System.out.println("The product you have scanned is not in the database, please wait while system updates the products");
						
					}else
					{
						//Add product to list of items being purchased
						transaction.addPurchaseItem(product);
					}
					
					System.out.println();
				}while(input != -1);
			}catch(IOException e)
			{
				System.out.println("Input Error");
			}	
		
		app = new Application();
		app.SlashInterface();
	}
	
	public void removePurchase(){
		System.out.println("*********************************************************************");
		System.out.println("*Welcome to Devastating Deliverables Store Purchasing Interface******");
		System.out.println("Please enter the products you would like to buy in the prompts boxes*");
		System.out.println("*********************************************************************");
		
		try{
			System.out.println("Enter Product id you wish to remove: ");
			input = Integer.parseInt(br.readLine());
			Product product = transaction.loadProduct(input, flag);
			transaction.removePurchase(product);
		}catch(IOException e){
			
		}
	}
}
