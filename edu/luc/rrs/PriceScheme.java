package edu.luc.rrs;




public class PriceScheme {

	private Integer pricingSchemeID;
	private Integer quantity;
	private double price;
	private String priceSchemeDesc;
	
	/**
	 * @return the pricingSchemeID
	 */
	public Integer getPricingSchemeID() {
		return pricingSchemeID;
	}
	/**
	 * @param pricingSchemeID the pricingSchemeID to set
	 */
	public void setPricingSchemeID(Integer pricingSchemeID) {
		this.pricingSchemeID = pricingSchemeID;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	/**
	 * @return the priceSchemeDesc
	 */
	public String getPriceSchemeDesc() {
		return priceSchemeDesc;
	}
	/**
	 * @param priceSchemeDesc the priceSchemeDesc to set
	 */
	public void setPriceSchemeDesc(String priceSchemeDesc) {
		this.priceSchemeDesc = priceSchemeDesc;
	}
}
