package edu.luc.rrs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;

public class Application {

	/**
	 * @param args
	 * @throws SQLException
	 */
	ArrayList<QuanPrice> quanPriceObj = new ArrayList<QuanPrice>();
	private int choice = 0;
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	@SuppressWarnings("unused")
	public static void main(String[] args) throws SQLException {

		Application application = new Application();
		boolean flag = true;	
		application.SlashInterface();
	}

	// Initialize and Populate the Database with Products
	// Transition to the transaction after initialization

	public void initializeSystem() {
		// List a few initial products that will be in the store

		// 1. Cosmetics

	}

	/**
	 * 
	 * Create Welcome Screen Interfaces
	 */
	public void SlashInterface() {
		
		try{
			do {
				// This is where Noura's code will go to display the screen to the user
				System.out.println("*******************************************");
				System.out.println("*Welcome to Devastating Deliverables Store*");
				System.out.println("*******************************************");
	
				System.out.println("Press 1 to start scanning products: ");
				System.out.println("Press 2 to remove scanned Item");
				System.out.println("Press 3 to complete purchasing: ");
				System.out.println("Press 4 to exit program");
				choice = Integer.parseInt(br.readLine());
				switch (choice) {
				case 1:
					Purchase purchase = new Purchase();
					purchase.purchasingInterface();
					break;
				case 2:
					purchase = new Purchase();
					purchase.removePurchase();
					break;
				case 3:
					Operations operation = new Operations();
					operation.completePurchase();
					break;
				case 4:
					System.out.println("Thank you for shopping with us!!");
					System.exit(0);
					break;
				default:
					System.exit(1);
					break;
				}
	
			} while (choice != -1);
			
		}catch(IOException e)
		{
			
		}
	}
}