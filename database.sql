
CREATE DATABASE /*!32312 IF NOT EXISTS*/`softwareengineering` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `softwareengineering`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `CategoryID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryDesc` varchar(200) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

insert  into `category`(`CategoryID`,`CategoryDesc`) values (1,'Books'),(2,'Stationary'),(3,'Electronics');

/*Table structure for table `priceschemeprices` */

DROP TABLE IF EXISTS `priceschemeprices`;

CREATE TABLE `priceschemeprices` (
  `PricingSchemeID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Price` float NOT NULL,
  PRIMARY KEY (`Quantity`,`PricingSchemeID`),
  KEY `PricingSchemeID` (`PricingSchemeID`),
  CONSTRAINT `priceschemeprices_ibfk_1` FOREIGN KEY (`PricingSchemeID`) REFERENCES `pricingscheme` (`PricingSchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `priceschemeprices` */

insert  into `priceschemeprices`(`PricingSchemeID`,`Quantity`,`Price`) values (90001,1,40),(90002,1,100),(90003,1,50),(90004,1,100),(90014,1,40),(90016,1,10),(90017,1,10),(90018,1,10),(90001,2,0),(90002,2,100),(90003,2,25),(90004,2,100),(90017,2,20),(90018,2,20),(90002,3,0),(90004,3,50),(90018,3,0);

/*Table structure for table `pricingscheme` */

DROP TABLE IF EXISTS `pricingscheme`;

CREATE TABLE `pricingscheme` (
  `PricingSchemeID` int(11) NOT NULL AUTO_INCREMENT,
  `PricingSchemeDesc` varchar(200) NOT NULL,
  PRIMARY KEY (`PricingSchemeID`)
) ENGINE=InnoDB AUTO_INCREMENT=90021 DEFAULT CHARSET=latin1;

/*Data for the table `pricingscheme` */

insert  into `pricingscheme`(`PricingSchemeID`,`PricingSchemeDesc`) values (90001,'Buy one get one free'),(90002,'Buy two get one free'),(90003,'test'),(90004,'test3 1111'),(90005,'Regular price'),(90014,'sunday night'),(90015,'Test Sunday'),(90016,'Test Sunday'),(90017,'Test Sunday'),(90018,'Test Sunday');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) NOT NULL,
  `PricingSchemeID` int(11) NOT NULL,
  `ProductDesc` varchar(200) NOT NULL,
  `IsTaxable` tinyint(1) NOT NULL,
  PRIMARY KEY (`ProductID`),
  UNIQUE KEY `productname` (`ProductDesc`),
  KEY `product_category_1` (`CategoryID`),
  KEY `product_ibfk_1` (`PricingSchemeID`),
  CONSTRAINT `product_category_1` FOREIGN KEY (`CategoryID`) REFERENCES `category` (`CategoryID`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`PricingSchemeID`) REFERENCES `pricingscheme` (`PricingSchemeID`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

/*Data for the table `product` */

insert  into `product`(`ProductID`,`CategoryID`,`PricingSchemeID`,`ProductDesc`,`IsTaxable`) values (1,1,90001,'TSPi',1),(2,1,90001,'Software Engineering 2nd edition',1),(3,3,90003,'iPad',1),(4,3,90004,'Laptop',1),(5,2,90001,'Eraser',1),(6,1,90001,'Intro to Computing',0),(7,1,90002,'TSPi test',1);

/* Procedure structure for procedure `CreatePriceScheme` */

/*!50003 DROP PROCEDURE IF EXISTS  `CreatePriceScheme` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CreatePriceScheme`(IN p_PricingSchemeDesc VARCHAR(200), out InsertedPriceSchemeID int )
BEGIN
   INSERT INTO PricingScheme(PricingSchemeDesc)
   VALUES(p_PricingSchemeDesc);
   
   
   SET InsertedPriceSchemeID = (SELECT LAST_INSERT_ID() FROM PricingScheme LIMIT 1);
   
END */$$
DELIMITER ;

/* Procedure structure for procedure `CreatePriceSchemePrices` */

/*!50003 DROP PROCEDURE IF EXISTS  `CreatePriceSchemePrices` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CreatePriceSchemePrices`(in p_PriceSchemeID int, in p_Quantity INT, in p_Price INT)
BEGIN
   INSERT INTO Priceschemeprices(PricingSchemeID, Quantity, Price)
   VALUES(p_PriceSchemeID, p_Quantity, p_Price);
 
   
END */$$
DELIMITER ;

/* Procedure structure for procedure `CreateProduct` */

/*!50003 DROP PROCEDURE IF EXISTS  `CreateProduct` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateProduct`(IN p_CategoryID INT, IN p_pricingSchemeID INT, IN p_ProductDesc VARCHAR(200), IN 
p_IsTaxable bool, out InsertedProductID int)
BEGIN
   INSERT INTO product(CategoryID, PricingSchemeID, ProductDesc, IsTaxable)
   VALUES(p_CategoryID, p_pricingSchemeID, p_ProductDesc, p_IsTaxable);
   
  
   SET InsertedProductID = (SELECT LAST_INSERT_ID() from Product limit 1);
   
END */$$
DELIMITER ;

/* Procedure structure for procedure `LoadPriceScheme` */

/*!50003 DROP PROCEDURE IF EXISTS  `LoadPriceScheme` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `LoadPriceScheme`(IN ID INT)
BEGIN
 SELECT ps.PricingSchemeID, psp.Quantity, psp.Price, ps.`PricingSchemeDesc`
FROM priceschemeprices psp
INNER JOIN pricingScheme ps
ON psp.`PricingSchemeID` = ps.`PricingSchemeID`  
  where ps.pricingSchemeID = id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `LoadProduct` */

/*!50003 DROP PROCEDURE IF EXISTS  `LoadProduct` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `LoadProduct`(IN ID INT)
BEGIN
   SELECT p.productID, p.ProductDesc, p.IsTaxable, c.CategoryDesc
   FROM Product p
   INNER JOIN Category c
   on p.CategoryID = c.CategoryID
   WHERe p.`ProductID` = ID;
END */$$
DELIMITER ;

/* Procedure structure for procedure `removePriceScheme` */

/*!50003 DROP PROCEDURE IF EXISTS  `removePriceScheme` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `removePriceScheme`(IN ID INT)
BEGIN
	delete from priceschemeprices
	where pricingSchemeID = id;
	
	delete from pricingScheme
	where pricingSchemeID = id;
END */$$
DELIMITER ;

/* Procedure structure for procedure `removeProduct` */

/*!50003 DROP PROCEDURE IF EXISTS  `removeProduct` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `removeProduct`(IN ID INT)
BEGIN
   DELETE FROM Product
   WHERE `ProductID` = ID;
END */$$
DELIMITER ;

/* Procedure structure for procedure `storePriceScheme` */

/*!50003 DROP PROCEDURE IF EXISTS  `storePriceScheme` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `storePriceScheme`(IN ID INT, in p_Quantity int, in p_Price float, in p_PricingSchemeDesc varchar
(200))
BEGIN
	update pricingScheme
	set PricingSchemeDesc = p_PricingSchemeDesc 
	where PricingSchemeID = id;
	
	Update priceschemeprices
	set Price = p_Price
	where PricingSchemeID = id
	and Quantity = p_Quantity;
END */$$
DELIMITER ;

/* Procedure structure for procedure `storeProduct` */

/*!50003 DROP PROCEDURE IF EXISTS  `storeProduct` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `storeProduct`(IN ID INT, in p_categoryID int, in p_PricingSchemeID int, in p_ProductDesc varchar
(200), in p_IsTaxable tinyint)
BEGIN
	update Product
	SET CategoryID = p_categoryID
	, PricingSchemeID = p_PricingSchemeID
	, ProductDesc = p_ProductDesc
	, IsTaxable = p_IsTaxable
	where ProductID = id;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
